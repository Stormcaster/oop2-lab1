package oop.lab1;

//TODO Write class Javadoc
/** 
 * Model for student with name and ID.
 * @author Pitchaya Namchaisiri
 * @version 2015.01.13
 *
 */
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	/**
	 * 
	 * @param name is the name of the new Student
	 * @param id is the ID of the new Student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	/**
	 * @param other is any other object to compare with this Student.
	 * @return true if the ID is same. False otherwise.
	 */
	public boolean equals(Object other) {
		if (other == null) return false;
		if (other.getClass() != this.getClass()) return false;
		Student another = (Student) other;
		if (id == another.id) return true;
		return false;
	}
}
